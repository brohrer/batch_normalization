import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches


# For maximum portability
plt.switch_backend("agg")

# For reproducibility
np.random.seed(23139)

# All positions and distances are in centimeters
fig_width = 16
fig_height = 9
plot_height = 5
plot_width = 6
side_border = .7
bottom_border = 2.4

small_plot_height = 2
small_plot_width = 3
small_bottom_border = 1
small_between_spacing_vert = .7
small_between_spacing_horz = 1
cm_per_in = 2.54

# Website color palette
tan = "#e1ddbf"
green = "#4c837a"
blue = "#04253a"

dpi = 300
tick_scale = .01
tick_label_scale = .03

bin_width = .2
epsilon = bin_width * 1e-3
hist_min = -3
hist_max = 3
arrow_scale = .7


def main():
    # Figure: activity distribution
    fig = create_fig()
    ax = add_one_ax(fig)
    activities = np.random.normal(size=1000)
    histogram(ax, activities)
    add_spines(ax)
    save(fig, "normal_dist.png")

    # Figure: activity distribution shifted
    fig = create_fig()
    ax = add_one_ax(fig)
    activities = np.random.normal(loc=-3, size=1000)
    histogram(ax, activities)
    add_spines(ax)
    save(fig, "normal_dist_shift.png")

    # Figure: activity distribution shifted
    fig = create_fig()
    ax = add_one_ax(fig)
    activities_1 = np.random.normal(loc=-2, size=1000)
    activities_2 = np.random.normal(loc=.7, scale=.5, size=1000)
    activities = np.concatenate((activities_1, activities_2))
    histogram(ax, activities)
    add_spines(ax)
    save(fig, "nonnormal_dist.png")

    # Figure: mean shift
    fig = create_fig()
    ax_1, ax_2 = add_two_axes(fig)
    activities = np.random.normal(loc=2, size=1000)
    shifted = activities - np.mean(activities)
    histogram(ax_1, activities)
    histogram(ax_2, shifted)
    match_limits([ax_1, ax_2])
    add_arrow(fig, "subtract mean")
    add_spines(ax_1)
    add_spines(ax_2)
    save(fig, "mean_removal.png")

    # Figure: rescaling
    fig = create_fig()
    ax_1, ax_2 = add_two_axes(fig)
    activities = np.random.normal(scale=2, size=1000)
    scaled = activities / np.sqrt(np.var(activities))
    histogram(ax_1, activities)
    histogram(ax_2, scaled)
    add_arrow(fig, "divide by\nstandard\ndeviation")
    match_limits([ax_1, ax_2])
    add_spines(ax_1)
    add_spines(ax_2)
    save(fig, "unit_var.png")

    # Figure: rescaling
    fig = create_fig()
    ax_1, ax_2 = add_two_axes(fig)
    activities_1 = np.random.normal(loc=2.2, size=1000)
    activities_2 = np.random.normal(loc=4.6, scale=.4, size=1000)
    activities_3 = np.random.normal(loc=0, scale=.3, size=300)
    activities = np.concatenate(
        (activities_1, activities_2, activities_3))
    normed = (activities - np.mean(activities)) / np.sqrt(np.var(activities))
    histogram(ax_1, activities)
    histogram(ax_2, normed)
    add_arrow(fig, "normalize")
    match_limits([ax_1, ax_2])
    add_spines(ax_1)
    add_spines(ax_2)
    save(fig, "irregular_normalized.png")

    # Figure: small batch draws
    batch_size = 64
    fig = create_fig()
    ax_1, ax_2, ax_3, ax_4, ax_5, ax_6 = add_six_axes(fig)
    activities_1 = np.random.normal(size=batch_size)
    activities_2 = np.random.normal(size=batch_size)
    activities_3 = np.random.normal(size=batch_size)
    activities_4 = np.random.normal(size=batch_size)
    activities_5 = np.random.normal(size=batch_size)
    activities_6 = np.random.normal(size=batch_size)
    histogram(ax_1, activities_1)
    histogram(ax_2, activities_2)
    histogram(ax_3, activities_3)
    histogram(ax_4, activities_4)
    histogram(ax_5, activities_5)
    histogram(ax_6, activities_6)
    match_limits([ax_1, ax_2, ax_3, ax_4, ax_5, ax_6])
    add_spines(ax_1)
    add_spines(ax_2)
    add_spines(ax_3)
    add_spines(ax_4)
    add_spines(ax_5)
    add_spines(ax_6)
    save(fig, "dist_variability.png")

    # Figure: multiple normalizations
    batch_size = 256
    fig = create_fig()
    ax_1, ax_2, ax_3, ax_4, ax_5, ax_6 = add_six_axes(fig)
    activities_1 = np.random.normal(loc=-1, scale=.6, size=batch_size)
    activities_2 = np.random.normal(loc=1.7, scale=1.1, size=batch_size)
    activities_3 = np.random.normal(loc=-.3, scale=2, size=batch_size)
    normed_1 = (
        activities_1 - np.mean(activities_1)) / np.sqrt(np.var(activities_1))
    normed_2 = (
        activities_2 - np.mean(activities_2)) / np.sqrt(np.var(activities_2))
    normed_3 = (
        activities_3 - np.mean(activities_3)) / np.sqrt(np.var(activities_3))
    histogram(ax_1, activities_1)
    histogram(ax_2, normed_1)
    histogram(ax_3, activities_2)
    histogram(ax_4, normed_2)
    histogram(ax_5, activities_3)
    histogram(ax_6, normed_3)
    add_3_arrows(fig)
    match_limits([ax_1, ax_2, ax_3, ax_4, ax_5, ax_6])
    add_spines(ax_1)
    add_spines(ax_2)
    add_spines(ax_3)
    add_spines(ax_4)
    add_spines(ax_5)
    add_spines(ax_6)
    save(fig, "element_wise_normalization.png")


def create_fig():
    fig = plt.figure(
        # Convert figure dimensions to inches.
        # This is what form figsize is expected to have.
        figsize=(fig_width / cm_per_in, fig_height / cm_per_in),
        facecolor=tan)
    return fig


def add_one_ax(fig):
    left = fig_width / 2 - plot_width / 2
    right = fig_width / 2 + plot_width / 2
    bottom = bottom_border
    top = bottom_border + plot_height
    ax = add_ax_at_positions(fig, (left, right, bottom, top))
    return ax


def add_two_axes(fig):
    bottom = bottom_border
    top = bottom_border + plot_height

    left_1 = side_border
    right_1 = side_border + plot_width
    ax_1 = add_ax_at_positions(fig, (left_1, right_1, bottom, top))

    right_2 = fig_width - side_border
    left_2 = fig_width - side_border - plot_width
    ax_2 = add_ax_at_positions(fig, (left_2, right_2, bottom, top))

    return ax_1, ax_2


def add_six_axes(fig):
    right_l = fig_width / 2 - small_between_spacing_horz / 2
    left_l = right_l - small_plot_width
    left_r = fig_width / 2 + small_between_spacing_horz / 2
    right_r = left_r + small_plot_width

    bottom = small_bottom_border
    top = bottom + small_plot_height
    ax_1 = add_ax_at_positions(fig, (left_l, right_l, bottom, top))
    ax_2 = add_ax_at_positions(fig, (left_r, right_r, bottom, top))

    bottom = top + small_between_spacing_vert
    top = bottom + small_plot_height
    ax_3 = add_ax_at_positions(fig, (left_l, right_l, bottom, top))
    ax_4 = add_ax_at_positions(fig, (left_r, right_r, bottom, top))

    bottom = top + small_between_spacing_vert
    top = bottom + small_plot_height
    ax_5 = add_ax_at_positions(fig, (left_l, right_l, bottom, top))
    ax_6 = add_ax_at_positions(fig, (left_r, right_r, bottom, top))

    return (ax_1, ax_2, ax_3, ax_4, ax_5, ax_6)


def add_ax_at_positions(fig, positions):
    """
    positions is a tuple of (left, right, bottom, top) in centimeters.
    This is a convenience function, saving come mental conversions
    necessary for using add_axes().
    """
    left, right, bottom, top = positions
    ax = fig.add_axes((
        left / fig_width,
        bottom / fig_height,
        (right - left) / fig_width,
        (top - bottom) / fig_height
    ))
    initialize_ax(ax)
    return ax


def initialize_ax(ax):
    ax.set_facecolor(tan)
    ax.spines["right"].set_visible(False)
    ax.spines["left"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["top"].set_visible(False)
    ax.tick_params(bottom=False, top=False, left=False, right=False)
    ax.tick_params(
        labelbottom=False, labeltop=False, labelleft=False, labelright=False)


def histogram(ax, activities):
    # Find the bin edges that cover this set of activities.
    min_val = np.minimum(np.min(activities), hist_min)
    max_val = np.maximum(np.max(activities), hist_max)
    # Make sure there is a bin centered on zero
    left_edge = np.floor(
        (min_val + bin_width / 2) / bin_width) * bin_width - bin_width / 2
    right_edge = np.ceil(
        (max_val - bin_width / 2) / bin_width) * bin_width + bin_width / 2
    bin_edges = np.arange(left_edge, right_edge + epsilon, bin_width)

    counts, _ = np.histogram(activities, bin_edges)
    assert counts.size == bin_edges.size - 1

    y_max = np.max(counts) * 1.05

    # Generate the histogram bars
    for i_bar, count in enumerate(counts):
        path = [
            [bin_edges[i_bar], 0],
            [bin_edges[i_bar], counts[i_bar]],
            [bin_edges[i_bar + 1], counts[i_bar]],
            [bin_edges[i_bar + 1], 0],
        ]
        ax.add_patch(patches.Polygon(
            path,
            facecolor=green,
            edgecolor=blue,
            linewidth=.5,
            zorder=-1,
        ))
    ax.set_xlim(bin_edges[0] - bin_width / 2, bin_edges[-1] + bin_width / 2)
    ax.set_ylim(0, y_max)


def add_arrow(fig, text):
    ax = fig.add_axes((0, 0, 1, 1))
    ax.set_facecolor("none")
    ax.spines["right"].set_visible(False)
    ax.spines["left"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["top"].set_visible(False)
    ax.set_xlim(0, fig_width)
    ax.set_ylim(0, fig_height)
    x_center = fig_width / 2
    y_center = fig_height / 2
    y_text = y_center - fig_height * .05

    ax.text(
        x_center,
        y_text,
        text,
        horizontalalignment="center",
        verticalalignment="top",
        fontsize=7,
        color=blue,
        zorder=1,
    )
    s = arrow_scale
    path = [
        [-s / 2, -s / 8],
        [-s / 2, s / 8],
        [s / 2, s / 8],
        [s / 2, s / 4],
        [s * .8, 0],
        [s / 2, -s / 4],
        [s / 2, -s / 8],
    ]
    path += np.array([[x_center, y_center]])
    ax.add_patch(patches.Polygon(
        path,
        facecolor=green,
        edgecolor=blue,
        linewidth=.5,
        zorder=0,
    ))


def add_3_arrows(fig):
    ax = fig.add_axes((0, 0, 1, 1))
    ax.set_facecolor("none")
    ax.spines["right"].set_visible(False)
    ax.spines["left"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["top"].set_visible(False)
    ax.set_xlim(0, fig_width)
    ax.set_ylim(0, fig_height)
    x_center = fig_width / 2
    y_centers = [fig_height / 4, fig_height / 2, 3 * fig_height / 4]

    s = arrow_scale / 2
    path_template = [
        [-s / 2, -s / 8],
        [-s / 2, s / 8],
        [s / 2, s / 8],
        [s / 2, s / 4],
        [s * .8, 0],
        [s / 2, -s / 4],
        [s / 2, -s / 8],
    ]
    for y_center in y_centers:
        path = path_template + np.array([[x_center, y_center]])
        ax.add_patch(patches.Polygon(
            path,
            facecolor=green,
            edgecolor=blue,
            linewidth=.5,
            zorder=0,
        ))


def add_spines(ax):
    x_min, x_max = ax.get_xlim()
    y_min, y_max = ax.get_ylim()

    ax.plot(
        [x_min, x_max],
        [0, 0],
        color="black",
        linewidth=1,
        zorder=0,
        solid_capstyle="round",
    )
    ax.plot(
        [0, 0],
        [y_min, y_max],
        color="black",
        linewidth=1,
        zorder=0,
        solid_capstyle="round",
    )
    # Ticks at whole numbers only
    min_tick = np.ceil(x_min)
    max_tick = np.floor(x_max)
    # Ensure no more than 9 ticks
    interval = np.ceil((max_tick - min_tick) / 9)

    # Ensure that zero is included
    min_tick = np.ceil((min_tick - epsilon) / interval) * interval
    max_tick = np.floor((max_tick + epsilon) / interval) * interval
    tick_locations = np.arange(min_tick, max_tick + epsilon, interval)

    tick_height = y_max * tick_scale
    tick_label_height = -y_max * tick_label_scale
    for tick in tick_locations:
        ax.plot(
            [tick, tick],
            [0, -tick_height],
            linewidth=.5,
            color=blue,
            zorder=0,
            clip_on=False,
            solid_capstyle="round",
        )
        ax.text(
            tick,
            tick_label_height,
            f"{int(tick)}",
            color=blue,
            horizontalalignment="center",
            verticalalignment="top",
            zorder=0,
            fontsize=6,
        )


def match_limits(ax_list):
    """
    Ensure that two sets of axes have the same limits.
    Use the most extreme in each case.
    """
    x_min = 1e10
    x_max = -1e10
    y_min = 1e10
    y_max = -1e10

    for ax in ax_list:
        x_min = np.minimum(ax.get_xlim()[0], x_min)
        x_max = np.maximum(ax.get_xlim()[1], x_max)
        y_min = np.minimum(ax.get_ylim()[0], y_min)
        y_max = np.maximum(ax.get_ylim()[1], y_max)

    for ax in ax_list:
        ax.set_xlim(x_min, x_max)
        ax.set_ylim(y_min, y_max)


def save(fig, filename):
    plt.savefig(
        filename,
        edgecolor=fig.get_edgecolor(),
        facecolor=fig.get_facecolor(),
        dpi=dpi)
    plt.close()


if __name__ == "__main__":
    main()
