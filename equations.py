import matplotlib
import matplotlib.pyplot as plt


plt.switch_backend("agg")
matplotlib.rcParams['text.usetex'] = True

in_per_cm = 1 / 2.54
fig_width = 16 * in_per_cm
fig_height = 9 * in_per_cm

# Website color palette
tan = "#e1ddbf"
green = "#4c837a"
blue = "#04253a"

dpi = 150


def main():
    # Equation: batch norm without beta and gamma
    fig, ax = initialize_figure()
    add_equation(
        ax,
        r"y_i = \frac{x_i - \mu}{\sigma}"
    )
    save("eq_batch_norm_no_affine.png")

    # Equation: batch norm with beta and gamma
    fig, ax = initialize_figure()
    add_equation(
        ax,
        r"y_i = \left ( \frac{x_i - \mu}{\sigma} \right ) \gamma + \beta"
    )
    save("eq_batch_norm.png")


def initialize_figure():
    fig = plt.figure(figsize=(fig_width, fig_height))
    ax = fig.add_axes((-.01, -.01, 1.02, 1.02), facecolor=tan)
    ax.set_xlim(-1, 1)
    ax.set_ylim(-1, 1)
    return fig, ax


def add_equation(ax, eqn):
    # Ensure 
    full_eqn = r"\[" + eqn + r"\]"
    ax.text(
        0, 0,
        full_eqn,
        color=blue,
        fontsize=36,
        horizontalalignment="center",
        verticalalignment="center",
    )


def save(filename):
    plt.savefig(filename, dpi=dpi)
    plt.close()


if __name__ == "__main__":
    main()
